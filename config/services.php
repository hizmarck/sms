<?php

return [
    /*
    |-------------------------------
    | sms configurations
    |-------------------------------
    |
    | Your Account SID, TOKEN from www.twilio.com/console
    | Use a trial number or buy a valid number for SMS.
     */
    'twilio' => [
        'sid'       => env('TWILIO_SID', null),
        'token'     => env('TWILIO_TOKEN', null),
        'number'    => env('TWILIO_NUMBER_FROM', null)
    ],
    /* ------------------------------
     * Google services
     * ------------------------------
     */
    'google' => [
        'application_name' 	            => env('GOOGLE_APPLICATION_NAME'),
        'developer_key' 	            => env('GOOGLE_DEVELOPER_KEY'),
        'api_web_firebase'              => env('GOOGLE_API_WEB_FIREBASE'),
        'domain_uri_prefix_firebase'    => env('GOOGLE_DOMAIN_URI_PREFIX_FIREBASE')
    ],
    /* -----------------------------
     * KiraSip
     * -----------------------------
     */
    'kiraSip' => [
        'api'       => env('KIRA_SIP_API_URI'),
        'user'      => env('KIRA_SIP_USER'),
        'password'  => env('KIRA_SIP_PASSWORD')
    ]
];
