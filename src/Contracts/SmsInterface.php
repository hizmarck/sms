<?php

namespace Hizmarck\SMS\Contracts;

interface SmsInterface {

    /**
     * Send sms with the indicated client.
     *
     * @param null $fromNumber some API need that indicated a from number. If the selected implementation don't need it, you can avoid this value.
     * @param $toNumber number to send the sms
     * @param $text string content of the sms
     * @return mixed
     */
    public function sendSMS($toNumber, $text, $fromNumber = null);

}
