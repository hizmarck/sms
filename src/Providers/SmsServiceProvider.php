<?php

/*
 * This file is part of sms services.
 *
 * (c) Sean hizmarck <hizmarck@gmail.com>
 *
 */

namespace Hizmarck\SMS\Providers;

use Hizmarck\SMS\Service\Google\UrlShorterEndPoint;
use Hizmarck\SMS\Service\Sms\KiraSipEndPoint;
use Hizmarck\SMS\Service\Sms\SmsTwilioEndPoint;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;
use Twilio\Rest\Client;

class SmsServiceProvider extends ServiceProvider
{

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $this->app->configure('services');

        $path = realpath(__DIR__ . '/../../config/services.php');

        $this->mergeConfigFrom($path, 'services');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Client::class, function (Application $app) {

            $twilioSid = $app->make('config')->get('services.twilio.sid');
            $twilioToken = $app->make('config')->get('services.twilio.token');

            return new Client($twilioSid, $twilioToken);
        });

        $this->app->bind(\Google_Client::class, function (Application $app) {

            $googleNameApp = $app->make('config')->get('services.google.application_name');
            $googleDevKey = $app->make('config')->get('services.google.developer_key');

            $client = new \Google_Client();
            $client->setApplicationName($googleNameApp);
            $client->setDeveloperKey($googleDevKey);

            return $client;
        });

        $this->app->bind(UrlShorterEndPoint::class, function (Application $app) {
            return new UrlShorterEndPoint($app->make(\Google_Client::class));
        });

        $this->app->bind(SmsTwilioEndPoint::class, function (Application $app) {
            return new SmsTwilioEndPoint($app->make(Client::class), $app->make('config')->get('services.twilio.number'));
        });

        $this->app->bind(KiraSipEndPoint::class, function (Application $app) {

            $kiraSipApi = $app->make('config')->get('services.kiraSip.api');
            $kiraSipUser = $app->make('config')->get('services.kiraSip.user');
            $kiraSipPassword = $app->make('config')->get('services.kiraSip.password');

            return new KiraSipEndPoint(new \GuzzleHttp\Client(), $kiraSipApi, $kiraSipUser, $kiraSipPassword);
        });
    }
}
