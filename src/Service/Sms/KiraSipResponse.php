<?php


namespace Hizmarck\SMS\Service\Sms;

/**
 * Response of KiraSip to sent sms.
 *
 * The value returned is a json {"status":"Enviado", "message_id": "1111111"}.
 * All possibles status messages:
 *
 * “Enviado”
 * “Usuario o password incorrecto”
 * “Creditos Insuficientes”
 * “error: alguna_descripcion”
 * “Codigo de error: algun_valor”
 * “El numero de celular debe tener al menos 12 digitos”“Usuario o password no valido”
 * “Servicio no disponible”
 * “Servidor no valido”
 * “Falta el nombre de usuario”
 * “Falta el password”
 * “Sender id no valido”
 * “Falta el numero de teléfono”
 * “Numero de telefono no valido”
 * “Red no soportada”
 * “Error 1: No se pudo conectar al Servidor”
 * “Error 8: no se pudo ejecutar el query”
 * “Error 13: no se pudo ejecutar el query”
 * “Error 14: no se pudo ejecutar el query”
 * “Error 15: no se pudo ejecutar el query”
 *
 * https://kirasip.com/api.html
 *
 * Class KiraSipResponse
 * @package Hizmarck\SMS\Service\Sms
 */
class KiraSipResponse
{
    /**
     * @var string $status status of the sms sent.
     */
    public $status;

    /**
     * @var string $message_id identifier for message.
     */
    public $message_id;

}