<?php


namespace Hizmarck\SMS\Service\Sms;

use Hizmarck\SMS\Contracts\SmsInterface;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Api\V2010\Account\MessageInstance;
use Twilio\Rest\Client;

/**
 * Endpoint to implement for https://www.twilio.com/
 *
 * Class SmsTwilioEndPoint
 * @package Hizmarck\SMS\Service\Sms
 */
class SmsTwilioEndPoint implements SmsInterface
{
    /**
     * @var Client $_client
     */
    private $_client;

    /**
     * @var string $_twilioNumber
     */
    private $_twilioNumber;

    /**
     * SmsTwilioEndPoint constructor.
     *
     * @param Client $client Twilio SDK client
     * @param $twilioNumberFrom string twilio number from
     */
    public function __construct(Client $client, $twilioNumberFrom)
    {
        $this->_client = $client;
        $this->_twilioNumber = $twilioNumberFrom;
    }

    /**
     * Send sms with the indicated client.
     * @param $fromNumber string|null the correct value is got from services.twilio.number or can be indicated directly fro this method.
     * @param $toNumber string number to be send the sms
     * @param $text string content of the sms.
     *
     * @return MessageInstance Newly created MessageInstance
     * @throws TwilioException When an HTTP error occurs.
     */
    public function sendSMS($toNumber, $text, $fromNumber = null)
    {
        return $this->_client->messages->create(
            $toNumber,
            array(
                'from' => is_null($fromNumber) ? $this->_twilioNumber : $fromNumber,
                'body' => $text
            )
        );
    }
}
