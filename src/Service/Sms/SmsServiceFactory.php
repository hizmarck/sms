<?php

namespace Hizmarck\SMS\Service\Sms;

use Hizmarck\SMS\Contracts\SmsInterface;

class SmsServiceFactory {

    const KEY_SMS_TWILIO_SERVICE = 'sms_twilio';

    /**
     * Create an object for sending SMS, that object belongs to one service to that purpose.
     *
     * @param $keyService
     * @return SmsInterface
     */
    public static function createSmsService($keyService = self::KEY_SMS_TWILIO_SERVICE) {
        $service = null;

        switch ($keyService) {
            case self::KEY_SMS_TWILIO_SERVICE:
            default:
                $service = new SmsTwilioEndPoint();
        }
        return $service;
    }

    private function __construct() {}

    private function __clone() {}

    private function __wakeup() {}

}
