<?php

namespace Hizmarck\SMS\Service\Sms;

use GuzzleHttp\Client;
use Hizmarck\SMS\Contracts\SmsInterface;
use JsonMapper;

/**
 * Endpoint to consume https://kirasip.com/api.html
 *
 * Class KiraSipEndPoint
 * @package Hizmarck\SMS\Service\Sms
 */
class KiraSipEndPoint implements SmsInterface
{
    /**
     * @var Client $_client
     */
    private $_client;

    /**
     * @var string $_user
     */
    private $_user;

    /**
     * @var string $_password
     */
    private $_password;

    /**
     * @var string $_apiURL
     */
    private $_apiURL;

    /**
     * KiraSipEndPoint constructor.
     * @param Client $client guzzle client
     * @param string $apiUrl uri for the api
     * @param string $user user for KiraSip
     * @param string $password password for KiraSip
     */
    public function __construct(Client $client, $apiUrl, $user, $password)
    {
        $this->_client = $client;
        $this->_apiURL = $apiUrl;
        $this->_password = $password;
        $this->_user = $user;
    }

    /**
     * Send sms with the indicated client.
     *
     * @param $fromNumber string|null this value is omitted for KiraSip.
     * @param $toNumber string number to send the SMS, if the number contains '+' it will be removed (+52 ~ 52)
     * @param $text string content of the sms.
     *
     * @return KiraSipResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonMapper_Exception
     */
    public function sendSMS($toNumber, $text, $fromNumber = null)
    {
        $response =  $this->_client->request('GET', $this->_apiURL, [
            'query' => [
                'usuario' => $this->_user,
                'password' => $this->_password,
                'numero' => str_replace('+','',$toNumber),
                'mensaje' => $text
            ]
        ]);

        $json = json_decode($response->getBody());
        $mapper = new JsonMapper();
        return $mapper->map($json, new KiraSipResponse());
    }

}
