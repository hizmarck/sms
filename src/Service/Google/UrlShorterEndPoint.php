<?php

namespace Hizmarck\SMS\Service\Google;

/**
 * Wrapper for Google services
 * @author Hizmarck
 */
class UrlShorterEndPoint
{

    const LINK_PREFIX = '/?link=';

    /**
     * @var \Google_Client $_google
     */
    private $_google;

    private $_prefixFirebase;

    public function __construct(\Google_Client $google)
    {
        $this->_google = $google;
        $this->_prefixFirebase = config('services.google.domain_uri_prefix_firebase');
    }

    /**
     * Return a shorter url for long url given.
     * If an error exist during conversion of url, the method return the original long url.
     *
     * @param $longUrl string example https://neurona.tech/
     * @return string
     */
    public function getShorterUrl($longUrl)
    {
        $response = null;

        try {
            $service = new \Google_Service_FirebaseDynamicLinks($this->_google);
            $request = new \Google_Service_FirebaseDynamicLinks_CreateShortDynamicLinkRequest();
            $suffix = new \Google_Service_FirebaseDynamicLinks_Suffix();
            $dynamicLinkInfo = new \Google_Service_FirebaseDynamicLinks_DynamicLinkInfo();

            $dynamicLinkInfo->setDynamicLinkDomain($this->_prefixFirebase);
            $dynamicLinkInfo->setLink($longUrl);
            $suffix->setOption("SHORT");
            $request->setSuffix($suffix);

            $request->setDynamicLinkInfo($dynamicLinkInfo);
            $response = $service->shortLinks->create($request)->getShortLink();
        } catch (\Exception $e) {
            $response = $longUrl;
        }

        return $response;
    }

    /**
     * Example of link: 'https://gdev.page.link/?link=https://www.wikipedia.org/'
     * @param $link
     * @return string
     */
    public function createLongDynamicLink($link)
    {
        $lengthLink = strlen($link);

        $uriPrefixFirebase = substr($this->_prefixFirebase, -$lengthLink) === '/' ? substr($this->_prefixFirebase, $lengthLink - 1) : $this->_prefixFirebase;

        return $uriPrefixFirebase . self::LINK_PREFIX . $link;
    }
}
